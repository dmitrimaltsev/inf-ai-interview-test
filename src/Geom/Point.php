<?php

namespace Geom;

class Point
{
    private $x;

    private $y;

    /**
     * Point constructor.
     * @param double $x
     * @param double $y
     */
    public function __construct($x, $y)
    {
        $this->x = (double) $x;
        $this->y = (double) $y;
    }

    /**
     * @return double
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @return double
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param Point $point
     * @return double
     */
    public function distanceTo(Point $point)
    {
        return sqrt(pow($point->getX() - $this->x, 2) + pow($point->getY() - $this->y, 2));
    }
}
