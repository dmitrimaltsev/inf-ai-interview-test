<?php

namespace Geom;

class Shape
{
    protected $vertices;

    protected $n;

    /**
     * Shape constructor.
     * @param Point[] $vertices
     */
    public function __construct(array $vertices)
    {
        $this->n = count($vertices);
        if ($this->n < 3) {
            throw new \InvalidArgumentException('Shape must have at least 3 vertices.');
        }

        $this->vertices = $vertices;
    }

    /**
     * @return double
     */
    public function perimeter()
    {
        $perimeter = 0;
        for ($i = 1; $i < $this->n; $i++) {
            $perimeter += $this->distanceBetweenTwoVertices($i - 1, $i);
        }
        $perimeter += $this->distanceBetweenTwoVertices(0, $this->n - 1);
        return $perimeter;
    }

    /**
     * @param int $i
     * @param int $j
     * @return double
     */
    public function diagonalLength($i, $j)
    {
        if ($i < 0 || $j < 0 || $i >= $this->n || $j >= $this->n) {
            throw new \RangeException('Vertex index is out of range.');
        }

        return $this->distanceBetweenTwoVertices($i, $j);
    }

    /**
     * @param int $i
     * @param int $j
     * @return double
     */
    private function distanceBetweenTwoVertices($i, $j)
    {
        return $this->vertices[$i]->distanceTo($this->vertices[$j]);
    }
}
