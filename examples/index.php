<?php

require dirname(__DIR__) . '/vendor/autoload.php';

use Geom\Point;
use Geom\Shape;

$triangle = new Shape([
    new Point(0, 0),
    new Point(0, 1),
    new Point(1, 0)
]);

echo 'Triangle perimeter equals ' . $triangle->perimeter() . PHP_EOL;

$square = new Shape([
    new Point(0, 0),
    new Point(0, 1),
    new Point(1, 1),
    new Point(1, 0)
]);

echo 'Square perimeter equals ' . $square->perimeter() . PHP_EOL;

echo 'Square diagonal length equals ' . $square->diagonalLength(0, 2) . PHP_EOL;
