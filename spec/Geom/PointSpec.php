<?php

namespace spec\Geom;

use Geom\Point;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class PointSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->beConstructedWith(0, 0);
        $this->shouldHaveType(Point::class);
    }

    function it_returns_coordinates()
    {
        $this->beConstructedWith(0, 0);
        $this->getX()->shouldReturn(0.0);
        $this->getY()->shouldReturn(0.0);
    }

    function it_calculates_distance()
    {
        $this->beConstructedWith(0, 0);
        $this->distanceTo(new Point(0, 1))->shouldReturn(1.0);
    }
}
