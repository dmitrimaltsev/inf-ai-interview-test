<?php

namespace spec\Geom;

use Geom\Point;
use Geom\Shape;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ShapeSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->beConstructedWith([]);
        $this->shouldHaveType(Shape::class);
    }

    function it_checks_number_of_vertices()
    {
        $this->beConstructedWith([
            new Point(0, 0),
            new Point(1, 1)
        ]);
        $this->shouldThrow(\InvalidArgumentException::class)->duringInstantiation();
    }

    function it_calculates_perimeter()
    {
        $this->beConstructedWith([
            new Point(0, 0),
            new Point(0, 1),
            new Point(1, 0)
        ]);
        $this->perimeter()->shouldReturnApproximately(3.414214, 1e-6);
    }

    function it_calculates_diagonal_length()
    {
        $this->beConstructedWith([
            new Point(0, 0),
            new Point(0, 1),
            new Point(1, 1),
            new Point(1, 0)
        ]);
        $this->diagonalLength(0, 2)->shouldReturnApproximately(1.414214, 1e-6);
    }

    function it_checks_if_vertex_index_is_out_of_range()
    {
        $this->beConstructedWith([
            new Point(0, 0),
            new Point(0, 1),
            new Point(1, 0)
        ]);
        $this->shouldThrow(\RangeException::class)->during('diagonalLength', [-1,  1]);
        $this->shouldThrow(\RangeException::class)->during('diagonalLength', [ 3,  1]);
        $this->shouldThrow(\RangeException::class)->during('diagonalLength', [ 1, -1]);
        $this->shouldThrow(\RangeException::class)->during('diagonalLength', [ 1,  3]);
    }
}
